# Twitter and Neo4j 

Twitter project, neo4j and data extracted from twitter.

## How to start

```bash
> git clone https://gitlab.com/d4497/docker/neo4j.git
> cd neo4j
> docker compose up -d
```

Go the browser: `http://localhost:7474/browser/`

## Folder structure

```
- neo4j
	- conf
	- data
	- import
	- logs
	- plugins
- docker-compose.yaml
```

## Interaction between python and neo4j

The py2neo page has this to say on the subject: 

When considering whether to use py2neo or the official Python Driver for Neo4j, there is a trade-off to be made. Py2neo offers a larger surface, with both a higher level API and an OGM, but the official driver provides mechanisms to work with clusters, such as automatic retries. If you are new to Neo4j, need an OGM, do not want to learn Cypher immediately, or require data science integrations, py2neo may be the better choice. If you are building a high-availability Enterprise application, or are using a cluster, you likely need the official driver.

OGM -> Object Graph Mapper (ORM but for graphs)

## Twitter as a graph

| ![Twitter as a graph](./img/twitter-graph-model.svg) |
| :----------------------------: |
| <b>Image from http://network.graphdemos.com/<b>  |


## References

* https://thibaut-deveraux.medium.com/how-to-install-neo4j-with-docker-compose-36e3ba939af0
* Env: https://neo4j.com/docs/operations-manual/current/reference/configuration-settings/
* Singleton pattern: geeksforgeeks.org/singleton-pattern-in-python-a-complete-guide/
* Neo4j reference to twitter: 
  * http://network.graphdemos.com/
  * https://guides.neo4j.com/sandbox/twitter/index.html
